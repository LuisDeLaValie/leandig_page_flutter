import 'package:flutter/material.dart';
import 'package:universal_html/html.dart' as html;

class Pagerovider with ChangeNotifier {
  PageController scrollControler = new PageController();

  final _pages = ["home", "about", "pricing", "contact", "location"];

  int _oldPage = 0;
  createScrollController(String routeName) {
    this.scrollControler =
        new PageController(initialPage: getPageIndex(routeName));

    this.scrollControler.addListener(() {
      final index = (this.scrollControler.page ?? 0).round();

      if (_oldPage != index) {
        _oldPage = index;

        final ruta = _pages[index];
        html.document.title = ruta;
        html.window.history.pushState(null, 'title', '#/$ruta');
      }
    });
  }

  int getPageIndex(String routeName) =>
      (_pages.indexOf(routeName) == -1) ? 0 : _pages.indexOf(routeName);

  void goTo(int index) {
    scrollControler.animateToPage(
      index,
      duration: Duration(milliseconds: 300),
      curve: Curves.easeInOut,
    );
  }
}
