import 'package:flutter/material.dart';
import 'package:landing_page/providers/page_provider.dart';
import 'package:landing_page/router/router.dart';
import 'package:provider/provider.dart';

void main() {
  Flurorouter.configureRoutes();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => Pagerovider()),
      ],
      child: MaterialApp(
        title: 'Material App',
        onGenerateRoute: Flurorouter.router.generator,
        initialRoute: '/home',
      ),
    );
  }
}
