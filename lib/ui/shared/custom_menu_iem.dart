import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomMenuItem extends StatefulWidget {
  final String tex;
  final Function() onPress;

  const CustomMenuItem({
    Key? key,
    required this.tex,
    required this.onPress,
  }) : super(key: key);

  @override
  _CustomMenuItemState createState() => _CustomMenuItemState();
}

class _CustomMenuItemState extends State<CustomMenuItem> {
  bool ishover = false;

  @override
  Widget build(BuildContext context) {
    return FadeIn(
      child: MouseRegion(
        onEnter: (_) => setState(() => ishover = true),
        onExit: (_) => setState(() => ishover = false),
        child: GestureDetector(
          onTap: () => widget.onPress(),
          child: AnimatedContainer(
            duration: Duration(
              milliseconds: 300,
            ),
            width: 150,
            height: 50,
            color: ishover ? Colors.pinkAccent : Colors.black,
            child: Center(
              child: Text(
                widget.tex,
                style: GoogleFonts.roboto(fontSize: 20, color: Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
