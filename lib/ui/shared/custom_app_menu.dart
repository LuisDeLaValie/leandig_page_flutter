import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:landing_page/providers/page_provider.dart';
import 'package:landing_page/ui/shared/custom_menu_iem.dart';
import 'package:provider/provider.dart';

class CustomAppMenu extends StatefulWidget {
  const CustomAppMenu({Key? key}) : super(key: key);

  @override
  _CustomAppMenuState createState() => _CustomAppMenuState();
}

class _CustomAppMenuState extends State<CustomAppMenu>
    with SingleTickerProviderStateMixin {
  bool isopen = false;

  late AnimationController controler;

  @override
  void initState() {
    super.initState();
    controler = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 200),
    );
  }

  @override
  Widget build(BuildContext context) {
    final pro = Provider.of<Pagerovider>(context, listen: false);

    return GestureDetector(
      onTap: () {
        if (isopen)
          controler.reverse();
        else
          controler.forward();
        setState(() {
          isopen = !isopen;
        });
      },
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          width: 150,
          height: isopen ? 308 : 50,
          color: Colors.black,
          child: Column(
            children: [
              _MenuTile(isopen: isopen, controler: controler),
              if (isopen) ...[
                CustomMenuItem(tex: 'Home', onPress: () => pro.goTo(0)),
                CustomMenuItem(tex: 'About', onPress: () => pro.goTo(1)),
                CustomMenuItem(tex: 'Pracin', onPress: () => pro.goTo(2)),
                CustomMenuItem(tex: 'Conatc', onPress: () => pro.goTo(3)),
                CustomMenuItem(tex: 'Location', onPress: () => pro.goTo(4)),
                SizedBox(height: 8),
              ]
            ],
          ),
        ),
      ),
    );
  }
}

class _MenuTile extends StatelessWidget {
  const _MenuTile({
    Key? key,
    required this.isopen,
    required this.controler,
  }) : super(key: key);

  final bool isopen;
  final AnimationController controler;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      height: 50,
      child: Row(
        children: [
          AnimatedContainer(
            duration: Duration(milliseconds: 200),
            curve: Curves.easeInOut,
            width: isopen ? 50 : 0,
          ),
          Text(
            "menu",
            style: GoogleFonts.roboto(color: Colors.white, fontSize: 18),
          ),
          Spacer(),
          AnimatedIcon(
            icon: AnimatedIcons.menu_close,
            color: Colors.white,
            progress: controler,
          ),
        ],
      ),
    );
  }
}
