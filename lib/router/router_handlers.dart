import 'package:fluro/fluro.dart';
import 'package:landing_page/providers/page_provider.dart';
import 'package:landing_page/ui/page/home_page.dart';
import 'package:provider/provider.dart';

final homeHandler = Handler(handlerFunc: (context, params) {
  final page = params['page']!.first;

  if (page != "/") {
    final pro = Provider.of<Pagerovider>(context!, listen: false);

    pro.createScrollController(page);

    return HomePage();
  }
});
